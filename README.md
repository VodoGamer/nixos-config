# ❄️ My NixOS Config

```bash
git clone git@gitlab.com:VodoGamer/nixos-config.git
mv nixos-config $HOME/nix # Config is supposed to be in the ~/nix directory
cd $HOME/nix
sudo nixos-rebuild switch --flake .
home-manager switch --flake .
```
Enjoy!

![Screenshot](./screenshot.png)
