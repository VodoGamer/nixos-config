{

  imports = [
    ./zsh.nix
    ./modules/bundle.nix
  ];

  home = {
    username = "vodogamer";
    homeDirectory = "/home/vodogamer";
    stateVersion = "23.11";
  };
}
