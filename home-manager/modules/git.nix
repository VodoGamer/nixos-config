{
  programs.git = {
    enable = true;
    userName  = "VodoGamer";
    userEmail = "vodogamer@yandex.ru";
    aliases = {
      ci = "commit";
      co = "checkout";
      st = "status";
    };
    extraConfig = {
      # Sign all commits using ssh key
      commit.gpgsign = true;
      gpg.format = "ssh";
      user.signingkey = "~/.ssh/id_ed25519.pub";
    };
  };
}
